FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y openjdk-17-jdk curl unzip

RUN curl -L -o gradle.zip https://services.gradle.org/distributions/gradle-7.5.1-bin.zip \
    && unzip gradle.zip \
    && rm gradle.zip \
    && mv gradle-7.5.1 /opt/gradle

ENV PATH="/opt/gradle/bin:${PATH}"

RUN apt-get install -y samtools bcftools

COPY . /home

RUN cd /home && gradle build --no-daemon

RUN ls /home

RUN mkdir /app

RUN cp /home/build/libs/diplomski-rad-backend-0.0.1-SNAPSHOT.jar /app

RUN ls /home

RUN mkdir /app/scripts

RUN cp /home/src/main/java/hr/fer/diplomskiradbackend/scripts/annotationFiltering.py /app/scripts/annotationFiltering.py

ENTRYPOINT ["java","-jar","/app/diplomski-rad-backend-0.0.1-SNAPSHOT.jar"]
