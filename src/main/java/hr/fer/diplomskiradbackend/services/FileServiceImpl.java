package hr.fer.diplomskiradbackend.services;

import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.dtos.FileUploadDTO;
import hr.fer.diplomskiradbackend.entities.File;
import hr.fer.diplomskiradbackend.repositories.FileRepository;
import hr.fer.diplomskiradbackend.services.interfaces.FileService;
import hr.fer.diplomskiradbackend.services.interfaces.ProcessingService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static hr.fer.diplomskiradbackend.constants.Constants.*;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    @Lazy
    private ProcessingService processingService;

    Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    @Override
    public List<File> findAll() {
        return fileRepository.findAll();
    }

    @Override
    public java.io.File getFilePathById(String id) {
        File referenceGenome = fileRepository.findById(id).orElseThrow();
        return new java.io.File(referenceGenome.getPath());
    }

    @Override
    public java.io.File getFileIndexById(String fileId) {
        String indexPath = fileRepository.findById(fileId)
                .orElseThrow()
                .getIndexPath();

        return new java.io.File(indexPath);
    }

    @Override
    public void addNewFile(MultipartFile file, FileUploadDTO fileInfo) throws IOException, InterruptedException {
        String path = getDestinationPathFromFileType(fileInfo.getFileType());

        java.io.File localFile = new java.io.File(path + file.getOriginalFilename());

        try {
            OutputStream os = new FileOutputStream(localFile);
            os.write(file.getBytes());
            os.flush();
            os.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Generate index
        String indexPath = generateIndex(path, file.getOriginalFilename());

        // Get sequences
        List<String> sequences = getSequences(path, localFile.getPath());

        String finalPath = localFile.getPath();
        if (!Objects.equals(path, ALIGNMENT_PATH)) {
            boolean isGzipped = FilenameUtils.getExtension(localFile.getPath()).equals("gz");

            if (!isGzipped) {
                finalPath = finalPath + ".gz";
            }
        }

        fileRepository.save(new File(null, fileInfo.getName(), finalPath, indexPath, fileInfo.getFileType(), sequences));
        logger.info(fileInfo.getName() + " successfully saved to the database");
    }

    @Override
    public void filterFile(FileFilterInfoDTO filterInfo) throws IOException, InterruptedException {
        File file = fileRepository.findById(filterInfo.getFileId())
                .orElseThrow();

        File newFile = filterFileBasedOnType(file, filterInfo);

        if (newFile.getName() == null) {
            logger.error("File type not recognized.");
        } else {
            fileRepository.save(newFile);
            logger.info(newFile.getName() + " successfully saved to the database");
        }
    }

    private String getDestinationPathFromFileType(String fileType) {
        return switch (fileType.toLowerCase(Locale.ROOT)) {
            case "alignment" -> ALIGNMENT_PATH;
            case "variant" -> VARIANT_PATH;
            case "annotation" -> ANNOTATION_PATH;
            default -> OTHER_PATH;
        };
    }

    private String generateIndex(String path, String fileName) {
        String fullFilePath = path + fileName;
        String indexPath = "";

        switch (path) {
            case ALIGNMENT_PATH -> {
                logger.info("Sorting and indexing started for " + fileName);
                indexPath = processingService.sortAndIndexAlignmentFile(fullFilePath);
            }
            case VARIANT_PATH -> {
                logger.info("Compressing and indexing for " + fileName + " started");
                indexPath = processingService.compressAndIndexVariantFile(fullFilePath);
            }
            default -> logger.info("Annotation file doesn't require indexing");
        }

        return indexPath;
    }

    private File filterFileBasedOnType(File file, FileFilterInfoDTO filterInfo) throws IOException, InterruptedException {
        return switch (file.getType()) {
            case "alignment" -> processingService.filterAlignmentFileByPosition(file.getPath(), filterInfo);
            case "variant" -> processingService.filterVariantFileByPosition(file.getPath(), filterInfo);
            case "annotation" -> processingService.filterAnnotationFileByPosition(file.getPath(), filterInfo);
            default -> new File(null, null, null, null, null, null);
        };
    }

    private List<String> getSequences(String path, String fullPath) throws IOException, InterruptedException {
        List<String> sequences = null;

        logger.info("Starting sequence extracting");

        switch (path) {
            case ALIGNMENT_PATH -> {
                sequences = processingService.getAlignmentSequences(fullPath);
            }
            case VARIANT_PATH -> {
                sequences = processingService.getVariantSequences(fullPath);
            }
            default -> sequences = processingService.getAnnotationSequences(fullPath);
        }

        return sequences;
    }
}
