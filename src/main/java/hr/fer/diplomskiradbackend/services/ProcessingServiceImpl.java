package hr.fer.diplomskiradbackend.services;

import hr.fer.diplomskiradbackend.constants.Constants;
import hr.fer.diplomskiradbackend.dtos.ConsensusSequenceDTO;
import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.entities.ReferenceGenome;
import hr.fer.diplomskiradbackend.services.interfaces.ProcessingService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ProcessingServiceImpl implements ProcessingService {
    Logger logger = LoggerFactory.getLogger(ProcessingServiceImpl.class);

    @Override
    public ReferenceGenome generateConsensusSequence(
            ConsensusSequenceDTO consensusSequence,
            ReferenceGenome referenceGenome,
            String variantFilePath) throws IOException, InterruptedException {

        String destinationPath = Constants.REFERENCE_GENOME_PATH + consensusSequence.getFileName() + ".fasta";

        int minimumQuality = consensusSequence.getMinQuality();
        int minimumDepth = consensusSequence.getMinDepth();

        ProcessBuilder pb = new ProcessBuilder("" +
                "bash",
                "-c",
                "bcftools consensus -f " + referenceGenome.getPath() + " --exclude 'INFO/DP<" + minimumDepth + " && QUAL<" + minimumQuality + "' "
                        + variantFilePath + " > " + destinationPath
        );

        pb.directory(new File(System.getProperty("user.dir")));
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);

        Process process = pb.start();
        int exitCode = process.waitFor();

        if (exitCode != 0) {
            logger.error("Error, process exited with code " + exitCode);
            throw new InterruptedException("Error, process exited with code " + exitCode);
        } else {
            logger.info("Finished generating" + consensusSequence.getFileName() + " with exit code " + exitCode);

            String indexPath = indexReferenceGenome(destinationPath);

            return new ReferenceGenome(null, consensusSequence.getFileName(), destinationPath, indexPath, referenceGenome.getSequences());
        }
    }

    @Override
    public String sortAndIndexAlignmentFile(String alignmentFilePath) {
        ProcessBuilder sortProcessBuilder = new ProcessBuilder(
                "samtools",
                "sort",
                alignmentFilePath,
                "-o",
                alignmentFilePath
        );

        ProcessBuilder indexProcessBuilder = new ProcessBuilder(
                "samtools",
                "index",
                alignmentFilePath
        );

        try {
            // Sorting process
            Process sortProcess = sortProcessBuilder.start();
            int sortProcessExitCode = sortProcess.waitFor();

            if (sortProcessExitCode != 0) {
                logger.error("Error, sorting process exited with code " + sortProcessExitCode);
                throw new InterruptedException("Error, sorting process exited with code " + sortProcessExitCode);
            }

            // Indexing process
            Process indexProcess = indexProcessBuilder.start();
            int indexingProcessExitCode = indexProcess.waitFor();

            if (indexingProcessExitCode != 0) {
                logger.error("Error, indexing process exited with code " + indexingProcessExitCode);
                throw new InterruptedException("Error, indexing process exited with code " + indexingProcessExitCode);
            }

            logger.info("Alignment file at " + alignmentFilePath + " successfully sorted and indexed!");
            return alignmentFilePath + ".bai";

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String compressAndIndexVariantFile(String variantFilePath) {
        ProcessBuilder compressProcessBuilder = new ProcessBuilder(
                "bcftools",
                "view",
                variantFilePath,
                "-Oz",
                "-o",
                variantFilePath + ".gz"
        );

        String indexPath = FilenameUtils.getExtension(variantFilePath).equals("gz") ? variantFilePath : variantFilePath + ".gz";

        ProcessBuilder indexProcessBuilder = new ProcessBuilder(
                "bcftools",
                "index",
                indexPath
        );

        try {
            boolean isGzipped = FilenameUtils.getExtension(variantFilePath).equals("gz");

            if (!isGzipped) {
                // Compressing process
                Process compressProcess = compressProcessBuilder.start();
                int compressProcessExitCode = compressProcess.waitFor();

                if (compressProcessExitCode != 0) {
                    logger.error("Error, compress process exited with code " + compressProcessExitCode);
                    throw new InterruptedException("Error, compress process exited with code " + compressProcessExitCode);
                }
            }

            // Indexing process
            Process indexProcess = indexProcessBuilder.start();
            int indexingProcessExitCode = indexProcess.waitFor();

            if (indexingProcessExitCode != 0) {
                logger.error("Error, indexing process exited with code " + indexingProcessExitCode);
                throw new InterruptedException("Error, indexing process exited with code " + indexingProcessExitCode);
            }

            logger.info("Variant file at " + variantFilePath + " successfully compressed and indexed!");
            return indexPath + ".csi";

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String indexReferenceGenome(String referenceGenomeFilePath) {
        ProcessBuilder indexProcessBuilder = new ProcessBuilder(
                "samtools",
                "faidx",
                referenceGenomeFilePath
        );

        try {
            // Indexing process
            Process indexProcess = indexProcessBuilder.start();
            int indexingProcessExitCode = indexProcess.waitFor();

            if (indexingProcessExitCode != 0) {
                logger.error("Error, indexing process exited with code " + indexingProcessExitCode);
                throw new InterruptedException("Error, indexing process exited with code " + indexingProcessExitCode);
            }

            logger.info("Reference genome at " + referenceGenomeFilePath + " successfully indexed!");
            return referenceGenomeFilePath + ".fai";

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ReferenceGenome filterReferenceByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException {
        String destinationPath = Constants.REFERENCE_GENOME_PATH + fileFilterInfo.getFileName() + ".fasta";
        String region = "";
        String[] command = null;

        if (fileFilterInfo.getFilterBySequence()) {
            region = fileFilterInfo.getSequence();
            command = new String[]{"bash", "-c", "samtools faidx " + path + "  " + region};
        } else {
            region = String.format("%s:%d-%d", fileFilterInfo.getSequence(), fileFilterInfo.getStartPosition(), fileFilterInfo.getEndPosition());
            String removePosition = String.format("%d-%d", fileFilterInfo.getStartPosition(), fileFilterInfo.getEndPosition());
            command = new String[]{"bash", "-c", "samtools faidx " + path + "  " + region + " | sed 's/:" + removePosition + "//'"};
        }

        ProcessBuilder pb = new ProcessBuilder(command);

        pb.directory(new File(System.getProperty("user.dir")));
        pb.redirectOutput(new File(destinationPath));
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);

        Process process = pb.start();
        int exitCode = process.waitFor();

        if (exitCode != 0) {
            logger.error("Error, process exited with code " + exitCode);
            throw new InterruptedException("Error, process exited with code " + exitCode);
        } else {
            logger.info("Finished filtering for " + fileFilterInfo.getFileName() + " with exit code " + exitCode);

            String indexPath = indexReferenceGenome(destinationPath);
            List<String> sequences = new ArrayList<>(Collections.singletonList(fileFilterInfo.getSequence()));

            return new ReferenceGenome(null, fileFilterInfo.getFileName(), destinationPath, indexPath, sequences);
        }
    }

    @Override
    public hr.fer.diplomskiradbackend.entities.File filterAlignmentFileByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException {
        String destinationPath = Constants.ALIGNMENT_PATH + fileFilterInfo.getFileName() + ".bam";
        String region = "";

        if (fileFilterInfo.getFilterBySequence()) {
            region = fileFilterInfo.getSequence();
        } else {
            region = String.format("%s:%d-%d", fileFilterInfo.getSequence(), fileFilterInfo.getStartPosition(), fileFilterInfo.getEndPosition());
        }

        ProcessBuilder pb = new ProcessBuilder("samtools", "view", "-b", path, region);

        pb.directory(new File(System.getProperty("user.dir")));
        pb.redirectOutput(new File(destinationPath));
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);

        Process process = pb.start();
        int exitCode = process.waitFor();

        if (exitCode != 0) {
            logger.error("Error, process exited with code " + exitCode);
            throw new InterruptedException("Error, process exited with code " + exitCode);
        } else {
            logger.info("Finished filtering for" + fileFilterInfo.getFileName() + " with exit code " + exitCode);

            String indexPath = sortAndIndexAlignmentFile(destinationPath);
            List<String> sequences = new ArrayList<>(Collections.singletonList(fileFilterInfo.getSequence()));

            return new hr.fer.diplomskiradbackend.entities.File(null, fileFilterInfo.getFileName(), destinationPath, indexPath, "alignment", sequences);
        }
    }

    @Override
    public hr.fer.diplomskiradbackend.entities.File filterVariantFileByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException {
        String destinationPath = Constants.VARIANT_PATH + fileFilterInfo.getFileName() + ".vcf.gz";
        String region = "";

        if (fileFilterInfo.getFilterBySequence()) {
            region = fileFilterInfo.getSequence();
        } else {
            region = String.format("%s:%d-%d", fileFilterInfo.getSequence(), fileFilterInfo.getStartPosition(), fileFilterInfo.getEndPosition());
        }

        ProcessBuilder pb = new ProcessBuilder("bcftools", "view", "-Oz", path, region);

        pb.directory(new File(System.getProperty("user.dir")));
        pb.redirectOutput(new File(destinationPath));
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);

        Process process = pb.start();
        int exitCode = process.waitFor();

        if (exitCode != 0) {
            logger.error("Error, process exited with code " + exitCode);
            throw new InterruptedException("Error, process exited with code " + exitCode);
        } else {
            logger.info("Finished filtering for" + fileFilterInfo.getFileName() + " with exit code " + exitCode);

            String indexPath = compressAndIndexVariantFile(destinationPath);
            List<String> sequences = new ArrayList<>(Collections.singletonList(fileFilterInfo.getSequence()));

            return new hr.fer.diplomskiradbackend.entities.File(null, fileFilterInfo.getFileName(), destinationPath, indexPath, "variant", sequences);
        }
    }

    @Override
    public hr.fer.diplomskiradbackend.entities.File filterAnnotationFileByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException {
        String destinationPath = Constants.ANNOTATION_PATH + fileFilterInfo.getFileName() + ".gff";
        String[] positions = {Integer.toString(fileFilterInfo.getStartPosition()), Integer.toString(fileFilterInfo.getEndPosition())};

        List<String> command = Arrays.asList(
                "python3",
                Constants.ANNOTATION_FILTER_SCRIPT_PATH,
                path,
                destinationPath,
                fileFilterInfo.getSequence(),
                positions[0],
                positions[1]
        );

        logger.info(String.valueOf(command));

        ProcessBuilder pb = new ProcessBuilder(command);
        Process process = pb.start();

        int exitCode = process.waitFor();

        if (exitCode != 0) {
            logger.error("Error, process exited with code " + exitCode);
            throw new InterruptedException("Error, process exited with code " + exitCode);
        } else {
            logger.info("Finished filtering for" + fileFilterInfo.getFileName() + " with exit code " + exitCode);
            List<String> sequences = new ArrayList<>(Collections.singletonList(fileFilterInfo.getSequence()));

            return new hr.fer.diplomskiradbackend.entities.File(null, fileFilterInfo.getFileName(), destinationPath, "", "annotation", sequences);
        }
    }

    @Override
    public List<String> getReferenceSequences(String path) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", "grep \"^>\" " + path + " | cut -f1 -d \" \" | tr -d \">\"");
        Process process = pb.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String sequence;
        List<String> sequences = new ArrayList<>();

        while ((sequence = reader.readLine()) != null) {
            sequences.add(sequence);
        }

        process.waitFor();
        reader.close();
        logger.info("Sequences extracted for " + path);

        return sequences;
    }

    @Override
    public List<String> getAlignmentSequences(String path) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", "samtools view -H " + path + " | grep \"^@SQ\" | cut -f2 | sed 's/SN://'");
        Process process = pb.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String sequence;
        List<String> sequences = new ArrayList<>();

        while ((sequence = reader.readLine()) != null) {
            sequences.add(sequence);
        }

        process.waitFor();
        reader.close();
        logger.info("Sequences extracted for " + path);

        return sequences;
    }

    @Override
    public List<String> getVariantSequences(String path) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder("bash", "-c", "bcftools view -h " + path + " | grep \"^##contig\" | sed 's/^##contig=<ID=\\([^,]*\\).*/\\1/'");
        Process process = pb.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String sequence;
        List<String> sequences = new ArrayList<>();

        while ((sequence = reader.readLine()) != null) {
            sequences.add(sequence);
        }

        process.waitFor();
        reader.close();
        logger.info("Sequences extracted for " + path);

        return sequences;
    }

    @Override
    public List<String> getAnnotationSequences(String path) throws IOException, InterruptedException {
        ProcessBuilder compressProcessBuilder = new ProcessBuilder(
                "gzip",
                "-c",
                path
        );

        compressProcessBuilder.redirectOutput(new File(path + ".gz"));

        String finalPath = FilenameUtils.getExtension(path).equals("gz") ? path : path + ".gz";

        ProcessBuilder pb = new ProcessBuilder("bash", "-c", " gzip -dc " + finalPath + " | grep -v '^#' | awk '{print $1}' | sort | uniq");

        boolean isGzipped = FilenameUtils.getExtension(path).equals("gz");

        if (!isGzipped) {
            Process compressProcess = compressProcessBuilder.start();
            int compressProcessExitCode = compressProcess.waitFor();

            if (compressProcessExitCode != 0) {
                logger.error("Error, compress process exited with code " + compressProcessExitCode);
                throw new InterruptedException("Error, compress process exited with code " + compressProcessExitCode);
            }
        }

        Process process = pb.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String sequence;
        List<String> sequences = new ArrayList<>();

        while ((sequence = reader.readLine()) != null) {
            sequences.add(sequence);
        }

        process.waitFor();
        reader.close();
        logger.info("Sequences extracted for " + finalPath);

        return sequences;
    }
}
