package hr.fer.diplomskiradbackend.services;

import hr.fer.diplomskiradbackend.entities.Session;
import hr.fer.diplomskiradbackend.repositories.SessionRepository;
import hr.fer.diplomskiradbackend.services.interfaces.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {
    @Autowired
    SessionRepository sessionRepository;

    @Override
    public void addNewSession(Session newSession) {
        sessionRepository.save(new Session(null, newSession.getName(), newSession.getData()));
    }

    @Override
    public List<Session> getAllSessions() {
        return sessionRepository.findAll();
    }
}
