package hr.fer.diplomskiradbackend.services;

import hr.fer.diplomskiradbackend.dtos.ConsensusSequenceDTO;
import hr.fer.diplomskiradbackend.dtos.FileUploadDTO;
import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.entities.ReferenceGenome;
import hr.fer.diplomskiradbackend.repositories.FileRepository;
import hr.fer.diplomskiradbackend.repositories.ReferenceGenomeRepository;
import hr.fer.diplomskiradbackend.services.interfaces.ProcessingService;
import hr.fer.diplomskiradbackend.services.interfaces.ReferenceGenomeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static hr.fer.diplomskiradbackend.constants.Constants.REFERENCE_GENOME_PATH;

@Service
public class ReferenceGenomeServiceImpl implements ReferenceGenomeService {
    @Autowired
    ReferenceGenomeRepository referenceGenomeRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    ProcessingService processingService;

    Logger logger = LoggerFactory.getLogger(ReferenceGenomeServiceImpl.class);

    @Override
    public List<ReferenceGenome> findAll() {
        return referenceGenomeRepository.findAll();
    }

    @Override
    public File getReferenceGenomePathById(String id) {
        ReferenceGenome referenceGenome = referenceGenomeRepository.findById(id).orElseThrow();
        return new File(referenceGenome.getPath());
    }

    @Override
    public void addNewReferenceGenome(MultipartFile file, FileUploadDTO referenceGenomeInfo) throws IOException, InterruptedException {
        java.io.File localFile = new java.io.File(REFERENCE_GENOME_PATH + file.getOriginalFilename());

        try {
            OutputStream os = new FileOutputStream(localFile);
            os.write(file.getBytes());
            os.flush();
            os.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Indexing
        String indexPath = processingService.indexReferenceGenome(localFile.getPath());

        // Get sequences
        List<String> sequences = processingService.getReferenceSequences(localFile.getPath());

        referenceGenomeRepository.save(new ReferenceGenome(null, referenceGenomeInfo.getName(), localFile.getPath(), indexPath, sequences));
        logger.info(file.getName() + " successfully saved to the database");
    }

    @Override
    public File getReferenceGenomeFileIndexById(String referenceGenomeId) {
        String indexPath = referenceGenomeRepository.findById(referenceGenomeId)
                .orElseThrow()
                .getIndexPath();

        return new java.io.File(indexPath);
    }

    @Override
    public void filterReferenceFile(FileFilterInfoDTO filterInfo) throws IOException, InterruptedException {
        String referencePath = referenceGenomeRepository.findById(filterInfo.getFileId())
                .orElseThrow()
                .getPath();

        ReferenceGenome newReferenceGenome = processingService.filterReferenceByPosition(referencePath, filterInfo);
        referenceGenomeRepository.save(newReferenceGenome);

        logger.info(newReferenceGenome.getName() + " successfully saved to the database");
    }

    @Override
    public void generateConsensusSequence(ConsensusSequenceDTO consensusSequence) throws IOException, InterruptedException {
        ReferenceGenome referenceGenome = referenceGenomeRepository
                .findById(consensusSequence.getReferenceGenomeId())
                .orElseThrow();

        String variantPath = fileRepository.findById(consensusSequence.getVariantFileId())
                .orElseThrow()
                .getPath();

        ReferenceGenome newReferenceGenome = processingService.generateConsensusSequence(consensusSequence, referenceGenome, variantPath);
        referenceGenomeRepository.save(newReferenceGenome);

        logger.info(newReferenceGenome.getName() + " successfully saved to the database");
    }
}
