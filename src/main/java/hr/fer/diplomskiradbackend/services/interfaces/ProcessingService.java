package hr.fer.diplomskiradbackend.services.interfaces;

import hr.fer.diplomskiradbackend.dtos.ConsensusSequenceDTO;
import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.entities.File;
import hr.fer.diplomskiradbackend.entities.ReferenceGenome;

import java.io.IOException;
import java.util.List;

public interface ProcessingService {
    ReferenceGenome generateConsensusSequence(ConsensusSequenceDTO consensusSequence, ReferenceGenome referenceGenome, String variantFilePath) throws IOException, InterruptedException;
    String sortAndIndexAlignmentFile(String alignmentFilePath);
    String compressAndIndexVariantFile(String variantFilePath);
    String indexReferenceGenome(String referenceGenomeFilePath);
    ReferenceGenome filterReferenceByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException;
    File filterAlignmentFileByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException;
    File filterVariantFileByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException;
    File filterAnnotationFileByPosition(String path, FileFilterInfoDTO fileFilterInfo) throws IOException, InterruptedException;
    List<String> getReferenceSequences(String path) throws IOException, InterruptedException;
    List<String> getAlignmentSequences(String path) throws IOException, InterruptedException;
    List<String> getVariantSequences(String path) throws IOException, InterruptedException;
    List<String> getAnnotationSequences(String path) throws IOException, InterruptedException;
}
