package hr.fer.diplomskiradbackend.services.interfaces;

import hr.fer.diplomskiradbackend.entities.Session;

import java.util.List;

public interface SessionService {
    void addNewSession(Session newSession);
    List<Session> getAllSessions();
}
