package hr.fer.diplomskiradbackend.services.interfaces;

import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.dtos.FileUploadDTO;
import hr.fer.diplomskiradbackend.entities.File;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileService {
    List<File> findAll();
    java.io.File getFilePathById(String id);
    java.io.File getFileIndexById(String fileId);
    void addNewFile(MultipartFile file, FileUploadDTO fileInfo) throws IOException, InterruptedException;
    void filterFile(FileFilterInfoDTO filterInfo) throws IOException, InterruptedException;
}
