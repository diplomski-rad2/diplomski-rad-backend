package hr.fer.diplomskiradbackend.services.interfaces;

import hr.fer.diplomskiradbackend.dtos.ConsensusSequenceDTO;
import hr.fer.diplomskiradbackend.dtos.FileUploadDTO;
import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.entities.ReferenceGenome;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ReferenceGenomeService {
    List<ReferenceGenome> findAll();
    File getReferenceGenomePathById(String id);
    void addNewReferenceGenome(MultipartFile file, FileUploadDTO referenceGenomeInfo) throws IOException, InterruptedException;
    File getReferenceGenomeFileIndexById(String referenceGenomeId);
    void filterReferenceFile(FileFilterInfoDTO filterInfo) throws IOException, InterruptedException;
    void generateConsensusSequence(ConsensusSequenceDTO consensusSequence) throws IOException, InterruptedException;
}
