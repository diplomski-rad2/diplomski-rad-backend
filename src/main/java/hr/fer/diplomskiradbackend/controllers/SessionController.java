package hr.fer.diplomskiradbackend.controllers;

import hr.fer.diplomskiradbackend.dtos.SessionDTO;
import hr.fer.diplomskiradbackend.entities.Session;
import hr.fer.diplomskiradbackend.services.interfaces.SessionService;
import hr.fer.diplomskiradbackend.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("session")
public class SessionController {

    @Autowired
    SessionService sessionService;

    @GetMapping()
    public List<SessionDTO> getSessions() {
        return ObjectMapperUtils.mapAll(sessionService.getAllSessions(), SessionDTO.class);
    }

    @PostMapping()
    public ResponseEntity saveSession(@RequestBody Session newSession) {
        sessionService.addNewSession(newSession);

        return new ResponseEntity(HttpStatus.CREATED);
    }
}
