package hr.fer.diplomskiradbackend.controllers;

import hr.fer.diplomskiradbackend.dtos.FileDTO;
import hr.fer.diplomskiradbackend.dtos.FileUploadDTO;
import hr.fer.diplomskiradbackend.services.interfaces.FileService;
import hr.fer.diplomskiradbackend.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static hr.fer.diplomskiradbackend.util.ResponseWrapper.createStreamResource;
import static hr.fer.diplomskiradbackend.util.ResponseWrapper.downloadResource;

@RestController
@RequestMapping("file")
public class FileController {

    @Autowired
    private FileService fileService;

    @GetMapping(value = "")
    public List<FileDTO> getFiles() {
        return ObjectMapperUtils.mapAll(fileService.findAll(), FileDTO.class);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<FileSystemResource> getFile(@PathVariable String id) {
        return createStreamResource(fileService.getFilePathById(id));
    }

    @GetMapping("/index/{fileId}")
    public ResponseEntity<FileSystemResource> getFileIndex(@PathVariable String fileId) {
        return createStreamResource(fileService.getFileIndexById(fileId));
    }

    @PostMapping(value = "/upload", consumes = {MediaType.APPLICATION_OCTET_STREAM_VALUE,
            MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity uploadFile(@RequestParam MultipartFile file,
                                     @RequestPart FileUploadDTO fileInfo) throws IOException, InterruptedException {

        fileService.addNewFile(file, fileInfo);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<ByteArrayResource> downloadReference(@PathVariable String id) throws IOException {
        return downloadResource(fileService.getFilePathById(id));
    }
}
