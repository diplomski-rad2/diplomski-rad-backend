package hr.fer.diplomskiradbackend.controllers;

import hr.fer.diplomskiradbackend.dtos.FileUploadDTO;
import hr.fer.diplomskiradbackend.dtos.ReferenceGenomeDTO;
import hr.fer.diplomskiradbackend.services.interfaces.ReferenceGenomeService;
import hr.fer.diplomskiradbackend.util.ObjectMapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static hr.fer.diplomskiradbackend.util.ResponseWrapper.createStreamResource;
import static hr.fer.diplomskiradbackend.util.ResponseWrapper.downloadResource;

@RestController
@RequestMapping("reference")
public class ReferenceGenomeController {

    @Autowired
    private ReferenceGenomeService referenceGenomeService;

    @GetMapping(value = "")
    public List<ReferenceGenomeDTO> getReferences() {
        return ObjectMapperUtils.mapAll(referenceGenomeService.findAll(), ReferenceGenomeDTO.class);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<FileSystemResource> getReference(@PathVariable String id) {
        return createStreamResource(referenceGenomeService.getReferenceGenomePathById(id));
    }

    @GetMapping("/index/{referenceGenomeId}")
    public ResponseEntity<FileSystemResource> getReferenceGenomeIndex(@PathVariable String referenceGenomeId) {
        return createStreamResource(referenceGenomeService.getReferenceGenomeFileIndexById(referenceGenomeId));
    }

    @PostMapping(value = "/upload", consumes = {MediaType.APPLICATION_OCTET_STREAM_VALUE,
            MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity uploadFile(@RequestParam MultipartFile file,
                                     @RequestPart FileUploadDTO fileInfo) throws IOException, InterruptedException {

        referenceGenomeService.addNewReferenceGenome(file, fileInfo);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<ByteArrayResource> downloadReference(@PathVariable String id) throws IOException {
        return downloadResource(referenceGenomeService.getReferenceGenomePathById(id));
    }
}
