package hr.fer.diplomskiradbackend.controllers;

import hr.fer.diplomskiradbackend.dtos.ConsensusSequenceDTO;
import hr.fer.diplomskiradbackend.dtos.FileFilterInfoDTO;
import hr.fer.diplomskiradbackend.services.interfaces.FileService;
import hr.fer.diplomskiradbackend.services.interfaces.ProcessingService;
import hr.fer.diplomskiradbackend.services.interfaces.ReferenceGenomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("processing")
public class ProcessingController {

    @Autowired
    ProcessingService processingService;

    @Autowired
    ReferenceGenomeService referenceGenomeService;

    @Autowired
    FileService fileService;

    @PostMapping("/consensus-sequence")
    public ResponseEntity generateConsensusSequence(@RequestBody ConsensusSequenceDTO consensusSequence) throws IOException, InterruptedException {
        referenceGenomeService.generateConsensusSequence(consensusSequence);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PostMapping("/reference-filter")
    public ResponseEntity filterReferenceGenome(@RequestBody FileFilterInfoDTO fileFilterInfoDTO) throws IOException, InterruptedException {
        referenceGenomeService.filterReferenceFile(fileFilterInfoDTO);

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PostMapping("/file-filter")
    public ResponseEntity filterFile(@RequestBody FileFilterInfoDTO fileFilterInfoDTO) throws IOException, InterruptedException {
        fileService.filterFile(fileFilterInfoDTO);

        return new ResponseEntity(HttpStatus.CREATED);
    }
}
