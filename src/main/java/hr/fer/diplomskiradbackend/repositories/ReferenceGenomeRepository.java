package hr.fer.diplomskiradbackend.repositories;

import hr.fer.diplomskiradbackend.entities.ReferenceGenome;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReferenceGenomeRepository extends MongoRepository<ReferenceGenome, String> {}
