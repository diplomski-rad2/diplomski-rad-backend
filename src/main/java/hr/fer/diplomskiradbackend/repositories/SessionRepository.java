package hr.fer.diplomskiradbackend.repositories;

import hr.fer.diplomskiradbackend.entities.Session;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SessionRepository extends MongoRepository<Session, String> {}
