package hr.fer.diplomskiradbackend.repositories;

import hr.fer.diplomskiradbackend.entities.File;
import org.springframework.data.mongodb.repository.MongoRepository;
public interface FileRepository extends MongoRepository<File, String> {}
