package hr.fer.diplomskiradbackend.constants;

public class Constants {
    public static final String REFERENCE_GENOME_PATH = "/app/uploaded/referenceGenomes/";
    public static final String ALIGNMENT_PATH = "/app/uploaded/files/alignment/";
    public static final String ANNOTATION_PATH = "/app/uploaded/files/annotation/";
    public static final String VARIANT_PATH = "/app/uploaded/files/variant/";
    public static final String OTHER_PATH = "/app/uploaded/files/other/";
    public static final String ANNOTATION_FILTER_SCRIPT_PATH = "/app/scripts/annotationFiltering.py";
}
