package hr.fer.diplomskiradbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@Document("reference_genomes")
public class ReferenceGenome {
    @Id
    private String id;

    private String name;
    private String path;
    private String indexPath;
    private List<String> sequences;
}
