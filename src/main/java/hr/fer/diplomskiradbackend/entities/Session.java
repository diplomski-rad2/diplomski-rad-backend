package hr.fer.diplomskiradbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@Document("sessions")
public class Session {

    @Id
    private String id;

    private String name;
    private String data;
}
