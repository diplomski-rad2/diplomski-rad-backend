package hr.fer.diplomskiradbackend.entities;

public enum Role {
    USER,
    ADMIN
}
