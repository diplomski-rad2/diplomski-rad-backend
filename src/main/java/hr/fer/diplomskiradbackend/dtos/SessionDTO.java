package hr.fer.diplomskiradbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SessionDTO {
    private String id;
    private String name;
    private String data;

    public SessionDTO(){}
}
