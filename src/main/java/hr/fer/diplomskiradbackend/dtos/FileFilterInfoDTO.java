package hr.fer.diplomskiradbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FileFilterInfoDTO {
    private String fileId;
    private String fileName;
    private String sequence;
    private Boolean filterBySequence;
    private int startPosition;
    private int endPosition;
}

