package hr.fer.diplomskiradbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FileDTO {
    private String id;
    private String name;
    private String type;
    private List<String> sequences;

    public FileDTO(){}
}
