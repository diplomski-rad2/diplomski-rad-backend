package hr.fer.diplomskiradbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ConsensusSequenceDTO {
    private String fileName;
    private String referenceGenomeId;
    private String variantFileId;
    private int minQuality;
    private int minDepth;
}
