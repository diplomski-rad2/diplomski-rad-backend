package hr.fer.diplomskiradbackend.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ReferenceGenomeDTO {
    private String id;
    private String name;
    private List<String> sequences;

    public ReferenceGenomeDTO(){}
}
