package hr.fer.diplomskiradbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiplomskiRadBackendApplication {
	public static void main(String[] args) {
		SpringApplication.run(DiplomskiRadBackendApplication.class, args);
	}

}
