from typing import List
import argparse
import gzip

parser = argparse.ArgumentParser(description="Filter annotation file by sequence and position.")

parser.add_argument("input_annotation_file", help="Annotation file for filtering.")
parser.add_argument("output_annotation_file", help="Filtered annotation file.")
parser.add_argument("sequence", help="Sequence.")
parser.add_argument("positions", nargs=2, help="Start and end position.")

args = parser.parse_args()

input_annotation_file: str = args.input_annotation_file
output_annotation_file: str = args.output_annotation_file
sequence: str = args.sequence
positions = tuple(args.positions)

# Check if the input file is gzipped
if input_annotation_file.endswith('.gz'):
    annotation_input = gzip.open(input_annotation_file, 'rt')
else:
    annotation_input = open(input_annotation_file, 'r')

# Check if the output file should be gzipped
if output_annotation_file.endswith('.gz'):
    annotation_output = gzip.open(output_annotation_file, 'wt')
else:
    annotation_output = open(output_annotation_file, 'w')

for line_num, line in enumerate(annotation_input):
    columns: List[str] = line.strip().split("\t")
    if columns[0] == sequence:
        columns[3], columns[4] = positions
        filtered_line: str = "\t".join(columns) + "\n"
        annotation_output.write(filtered_line)
       
annotation_input.close()
annotation_output.close()